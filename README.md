# Lexicon Classifier using Doc2Vec and K-means #

Nick Glyder, Luke Morrow

## Required Software ##
* Python 3.6+
* PIP Packages:
..* NLTK 
..* SKlearn
..* Gensim

## Included Data ##
We have included parsed and prepared data sets for the Cornell and IMDB movie review corpi.
Check the paper + Eisenstein's paper for links and more info.
