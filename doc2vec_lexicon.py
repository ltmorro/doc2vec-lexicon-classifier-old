from doc2vec_process_data import load_data, load_lexicons
from doc2vec_model import doc2vec_train_model,\
                          generate_documents,\
                          generate_documents_lex_only
from doc2vec_classifier import logistic_doc2vec_regression,\
                               k_nearest_neighbor, k_means
from document import Document, DocType
import sys

# Based on a few stack overflow answers, just log.writes to terminal and file
class Logger(object):
    def __init__(self, log):
        self.terminal = sys.stdout
        self.log = open(log, 'a')
    def write(self, msg):
        self.terminal.write(msg)
        self.log.write(msg)
        return self

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('prefix')
parser.add_argument('poslex')
parser.add_argument('neglex')
parser.add_argument('--csv',          type=str)
parser.add_argument('--log',          default='doc2vec.log',type=str)
parser.add_argument('--epochs',       default=10,type=int)
parser.add_argument('--max_alpha',    default=0.05,type=float)
parser.add_argument('--min_alpha',    default=0.001,type=float)
parser.add_argument('--window',       default=5,type=int)
parser.add_argument('--vector_size',  default=100,type=int)
parser.add_argument('--workers',      default=4,type=int)
parser.add_argument('--lex_filter',   default=False,type=bool)
parser.add_argument('--verbosity',    default=0,type=int)
global args

if __name__ == '__main__':
    # Get access to CLA
    global args
    args = parser.parse_args()

    # Open loggers
    log = Logger(args.log)
    if args.csv is not None:
        csv = Logger(args.csv)
    else:
        csv = None

    # Load lexicon
    pos_lex, neg_lex = load_lexicons(pos_lex_path=args.poslex,
                                     neg_lex_path=args.neglex)
    if args.verbosity:
        log.write("{0} Positive Lexicon Tokens\n".format(len(pos_lex)))
        log.write("{0} Negative Lexicon Tokens\n\n".format(len(neg_lex)))

    # Load raw review data
    data = load_data(args.prefix)

    # Generate Document POPOs and TaggedDocs
    # If set to filter, will only use lexicon vocab
    if args.lex_filter:
        documents = generate_documents_lex_only(data, pos_lex, neg_lex)
    else:
        documents = generate_documents(data)

    # Quick lookup table for classifiers
    docmap = {d.filename : d for d in documents}

    # Split on true labels
    pos_docs = sum(1 for d in documents if d.doctype & DocType.POS)
    neg_docs = sum(1 for d in documents if d.doctype & DocType.NEG)
    if args.verbosity:
        log.write(f"{pos_docs} Positive Documents")
        log.write(f"{neg_docs} Negative Documents\n")

    # Train 90% of positive data
    pos_docs = list(filter(lambda d: d.doctype & DocType.POS, documents))
    for doc in pos_docs[:int(len(pos_docs)*.9)]:
        doc.doctype |= DocType.TRAIN
    for doc in pos_docs[int(len(pos_docs)*.9):]:
        doc.doctype |= DocType.TEST

    neg_docs = list(filter(lambda d: d.doctype & DocType.NEG, documents))
    for doc in neg_docs[:int(len(neg_docs)*.9)]:
        doc.doctype |= DocType.TRAIN
    for doc in neg_docs[int(len(neg_docs)*.9):]:
        doc.doctype |= DocType.TEST

    log.write("hyperparameters:")
    log.write(f'''
    epochs = {args.epochs}
    window = {args.window}
    vector_size = {args.vector_size}
    alpha = {args.max_alpha}\n\n''')

    # Train Doc2Vec feature space
    # Note, Doc2Vec is trained on ALL data since it is unsupervised.
    # The above split is used to train and evaluate the other classifiers
    model = doc2vec_train_model(documents, args.epochs,
        window=args.window, vector_size=args.vector_size,
        max_alpha=args.max_alpha, min_alpha=args.min_alpha, workers=args.workers)

    log.write("{0:<30}{1:<10}\n".format("classifier", "acc"))
    log.write("-".ljust(40,'-')).write("\n")

    # Use doc embeddings + labels to train logistic regression
    log_acc = logistic_doc2vec_regression(model, documents)
    log.write("{0:<30}{1:<10}\n".format("Logistic Regression", log_acc))

    # Cosine similarity
    knn_acc = k_nearest_neighbor(model, documents, docmap)
    log.write("{0:<30}{1:<10}\n".format("K Nearest Neighbor", knn_acc))

    # Use doc embeddings to train k means cluster
    km_acc = k_means(model, documents)
    log.write("{0:<30}{1:<10}\n\n".format("K Means", km_acc))

    # append to CSV if present
    if csv is not None:
        csv.write(f"{args.window},{args.vector_size},{log_acc},{knn_acc},{km_acc}\n")
        log.write("\n")
