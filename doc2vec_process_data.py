import os, codecs
import nltk
from nltk.tokenize import word_tokenize,sent_tokenize
nltk.download('punkt')
from glob import glob

from document import Document, DocType

# Same parsing procedure as the original paper for consistency
# https://www.cs.cornell.edu/people/pabo/movie-review-data/
cornell_base = 'txt_sentoken'

# Simple NLTK punkt tokenization and normalization
# Per Eisenstein and Cornell, use UTF-8 encoding
# Same tokinzation as original paper with no stopword removal
def tokenizeFile(filename):
    sentence = []
    with codecs.open(filename, mode='r', encoding='utf8') as data:
        for line in data:
            sentence += [x.lower() for x in word_tokenize(line) if x.isalpha()]
    return (" ".join(sentence), len(sentence))

# Parse Cornell movie reviews, return max record length
# 1-to-1 between sentences and key
def parseCornell(outprefix):
    max_size = 0;
    negdocs = glob(os.path.join(cornell_base,'neg/*.txt'))
    posdocs = glob(os.path.join(cornell_base,'pos/*.txt'))
    with open(outprefix+'.sent','w') as sentences:
        with open(outprefix+'.key','w') as sentences_key:
            for filename in posdocs:
                sentence, length = tokenizeFile(filename)
                if length > max_size:
                    max_size = length
                sentences_key.write(f"{filename} POS {length}\n")
                sentences.write(f"{sentence}\n")
            for filename in negdocs:
                sentence, length = tokenizeFile(filename)
                if length > max_size:
                    max_size = length
                sentences_key.write(f"{filename} NEG {length}\n")
                sentences.write(f"{sentence}\n")
    return max_size

# Load previously processed docs
def load_data(inputprefix):
    with open(f"{inputprefix}.sent", "r") as sentences:
        with open(f"{inputprefix}.key", "r") as key:
            # Match documents with keys
            sentences = [line for line in sentences.readlines()]
            keys      = [tuple(line.split()) for line in key.readlines()]
            assert len(sentences) == len(keys)
            return zip(sentences, keys)

# Return tuple with positive and negative lexicon dicts
def load_lexicons(pos_lex_path, neg_lex_path):
    pos_words = []
    neg_words = []

    with codecs.open(pos_lex_path,"r", encoding="utf8") as pos_lexicon:
        pos_words = [word for word in pos_lexicon.read().splitlines()]

    with codecs.open(neg_lex_path,"r", encoding="utf8") as neg_lexicon:
        neg_words = [word for word in neg_lexicon.read().splitlines()]

    pos_lex = {word for word in pos_words}
    neg_lex = {word for word in neg_words}
    return (pos_lex, neg_lex)
