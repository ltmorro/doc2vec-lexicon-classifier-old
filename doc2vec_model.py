from gensim.models.doc2vec import TaggedDocument
from gensim import models

from document import Document, DocType

from random import shuffle

def generate_documents(data):
    documents = []
    for sentence, key in data:
        filename, label, length = key
        doctype = DocType.POS if label == 'POS' else DocType.NEG
        documents.append(Document(
            filename=filename, doctype=doctype,
            length=length, text=sentence
        ))
    return documents

def generate_documents_lex_only(data, pos_lex, neg_lex):
    in_lex = lambda w: w in pos_lex or w in neg_lex
    documents = []
    for sentence, key in data:
        filename, label, length = key
        doctype = DocType.POS if label == 'POS' else DocType.NEG
        sentence = " ".join(filter(in_lex, sentence.split()))
        documents.append(Document(
            filename=filename, doctype=doctype,
            length=length, text=sentence
        ))
    return documents

def doc2vec_train_model(documents, epochs, window=5, vector_size=100,
                        max_alpha=0.05, min_alpha=0.001, workers=2):
    # Generate model
    model = models.Doc2Vec(min_count=1, window=window,
                           vector_size=vector_size, hs=1, workers=workers)
    train = list(map(lambda d: d.tagged_doc, documents))
    model.build_vocab(train)
    alpha = max_alpha
    delta = (max_alpha-min_alpha)/epochs
    print("Training Doc2Vec Model")
    for epoch in range(epochs):
        percent = 100*((epoch+1)/epochs)
        print(" %2.2f%% (a = %.4f)" % (percent, alpha), end='\r')
        shuffle(train)
        model.alpha, model.min_alpha = alpha, alpha
        model.train(train, total_examples=model.corpus_count, epochs=1)
        alpha -= delta
    print("\nTraining Done\n")
    return model
