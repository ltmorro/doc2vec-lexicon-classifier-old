EPOCHS=20
LEARNING=0.025
LOG=hyper_param.log
CSV=hyper_param.csv

rm -f $LOG $CSV

for window in 1 2 3 4 5; do
	for vec in 50 100 150 200; do
		for run in 1 2 3 4 5; do
			echo "window: $window vec: $vec run: $run"
			python doc2vec_lexicon.py data/cornell \
			data/liu-pos.utf8 data/liu-neg.utf8 --epochs=$EPOCHS \
			--window=$window --max_alpha=$LEARNING --lex_filter=True \
			--vector_size=$vec --workers=2 --log=$LOG --csv=$CSV 2>&1 > /dev/null
		done
	done
done
