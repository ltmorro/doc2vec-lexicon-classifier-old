from enum import Enum, Flag, auto
from gensim.models.doc2vec import TaggedDocument
from gensim import models

class DocType(Flag):
    POS = auto()
    NEG = auto()
    TRAIN = auto()
    TEST = auto()

class Document(object):
    """Represents a labeled doc + Doc2Vec representation. """

    def __init__(self, filename, doctype, length, text):
        self.filename = filename
        self.doctype = doctype
        self.label = 1 if self.doctype & DocType.POS else 0
        self.length = length
        self.tagged_doc = TaggedDocument(text.split(), [filename])
        return
